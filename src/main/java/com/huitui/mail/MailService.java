package com.huitui.mail;

import com.taobao.api.internal.util.StringUtils;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;

/**
 * Author: gang.pan Date: 7/18/16 Time: 15:54 Mail: gang.pan@htmob.cn
 */
public class MailService {
    private static final String MAIL_ACCOUNT = "shuai.xiong@htmob.cn";
    private static final String MAIL_PASS = "xs15073722800";
    private static final String SMTP_SERVER = "smtp.263.net";

    private MailService() {
    }

    /**
     * @param mailAddress 邮件接收人地址 多邮件,分割
     * @param subject 邮件主题
     * @param content 邮件内容
     **/
    public static void sendMail(String mailAddress, String subject, String content) throws MessagingException {

        sendEmail(mailAddress, subject, content, null);
    }

    public static void sendMailAttachment(String mailAddress, String subject, String content, String filePath)
            throws MessagingException {

        sendEmail(mailAddress, subject, content, filePath);
    }

    private static void sendEmail(String mailAddress, String subject, String content, String filePath)
            throws MessagingException {
        Properties props = new Properties();
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.host", SMTP_SERVER);
        props.setProperty("mail.transport.protocol", "smtp");
        Session session = Session.getInstance(props);
        MimeMessage msg = new MimeMessage(session);
        msg.setSubject(subject);
        msg.setText(content);
        msg.setFrom(new InternetAddress(MAIL_ACCOUNT));
        msg.setRecipients(Message.RecipientType.TO, getAddress(mailAddress));
        if (!StringUtils.isEmpty(filePath)) {
            // 创建邮件的各个 MimeBodyPart 部分
            MimeBodyPart bodyPart = createAttachment(filePath);
            MimeMultipart multipart = new MimeMultipart("mixed");
            multipart.addBodyPart(bodyPart);
            msg.setContent(multipart);
            msg.saveChanges();
        }
        Transport transport = session.getTransport();
        transport.connect(MAIL_ACCOUNT, MAIL_PASS);
        transport.sendMessage(msg, msg.getAllRecipients());
        transport.close();
    }

    /**
     * 获取邮件地址
     */
    private static InternetAddress[] getAddress(String mailAddress) throws AddressException {
        ArrayList<InternetAddress> list = new ArrayList<>();
        String[] mailSend = mailAddress.split(",");
        for (String mailTo : mailSend) {
            list.add(new InternetAddress(mailTo));
        }
        return list.toArray(new InternetAddress[list.size()]);
    }

    /**
     * 根据传入的文件路径创建附件并返回
     */
    private static MimeBodyPart createAttachment(String fileName) throws MessagingException {
        MimeBodyPart attachmentPart = new MimeBodyPart();
        FileDataSource fds = new FileDataSource(fileName);
        attachmentPart.setDataHandler(new DataHandler(fds));
        attachmentPart.setFileName(fds.getName());
        return attachmentPart;
    }
}
