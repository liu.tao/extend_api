package com.huitui.sms;

import com.google.gson.Gson;

/**
 * Author: gang.pan
 * Date: 6/30/16
 * Time: 17:02
 * Mail: gang.pan@htmob.cn
 */
public class ActiveSmsVO {
    private String appName;
    private String activeData;
    private String concealData;
    private String activePercent;
    private String total;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getConcealData() {
        return concealData;
    }

    public void setConcealData(String concealData) {
        this.concealData = concealData;
    }
    public String getActivePercent() {
        return activePercent;
    }

    public void setActivePercent(String activePercent) {
        this.activePercent = activePercent;
    }

    public String getActiveData() {
        return activeData;
    }

    public void setActiveData(String activeData) {
        this.activeData = activeData;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
    @Override
    public String toString(){
        return new Gson().toJson(this);
    }

}
