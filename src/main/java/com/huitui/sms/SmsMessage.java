package com.huitui.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * Author: gang.pan Date: 6/27/16 Time: 16:43 Mail: gang.pan@htmob.cn
 */
public class SmsMessage {
    private static final String APP_KEY = "23398262";
    private static final String APP_SECRET = "04b9b5636b4b5a308afdb280a8fed891";
    private static final String HTTP_SMS_URL = "http://gw.api.taobao.com/router/rest";
    private static final String SMS_SIGN = "慧推移动";
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsMessage.class);

    /**
     * @param extend 回传参数
     * @param phoneNumber 短信接收号码,多个手机号 , 分隔
     * @param paramString 短信参数json结构
     * @param smsTemplateCode 短信模板 .如果为空 则采用默认短信模板
     *
     * **/
    public static boolean sendSms(String extend, String phoneNumber, String paramString, String smsTemplateCode)
            throws ApiException {
        TaobaoClient client = new DefaultTaobaoClient(HTTP_SMS_URL, APP_KEY, APP_SECRET);
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend(extend);
        req.setSmsType("normal");
        req.setSmsFreeSignName(SMS_SIGN);
        req.setSmsParamString(paramString);
        req.setRecNum(phoneNumber);
        req.setSmsTemplateCode(smsTemplateCode);
        AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
        LOGGER.info("sms_info={},phone={},sms_code={}", paramString, phoneNumber, rsp.getErrorCode());
        return rsp.isSuccess();
    }

}
