package com.huitui.extend.entry;

/**
 * Created by renguanghui on 16-5-21.
 */
public enum ExchangeStatusEnum {
    NOT_FINISHED("申请成功待到账", 1), FINISHED("到账成功", 2);
    private int code;
    private String desc;

    ExchangeStatusEnum(String desc, int code) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(final int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }
}
