package com.huitui.extend.entry;

import java.util.Collection;
import java.util.Map;

/**
 * Author: gang.pan Date: 8/25/16 Time: 10:52 Mail: gang.pan@htmob.cn
 */
public class CreativeData {

    public static final String CREATIVE_TASK_LOCK_KEY = "creative_data_today";
    private static final String CREATIVE_CACHE_KEY = "creative_";
    public static final String CREATIVE_ACTIVE_DATA_KEY = "creative_active_data";
    public static final String CREATIVE_ACTIVE_REDIS_MESSAGE_MQ = "redisMQ_active_list";
    public static final String ACTIVE_UPDATE_TIME = "active_update_time";


    private int unActiveCount;
    private int activeCount;
    private int totalCount;
    private String creativeId;
    private Integer adid;
    private String mediaSource;

    public Integer getAdid() {
        return adid;
    }

    public String buildSecondCacheKey(String time) {
        return time + "_" + creativeId;
    }

    public String buildFirstCacheKey(String date) {
        return CREATIVE_CACHE_KEY + adid + "_" + date;
    }

    public static String getFirstCacheKey(String adid,String date) {
        return CREATIVE_CACHE_KEY + adid + "_" + date;
    };

    public void setAdid(Integer adid) {
        this.adid = adid;
    }

    public String getMediaSource() {
        return mediaSource;
    }

    public void setMediaSource(String mediaSource) {
        this.mediaSource = mediaSource;
    }

    public int getUnActiveCount() {
        return unActiveCount;
    }

    public void setUnActiveCount(int unActiveCount) {
        this.unActiveCount = unActiveCount;
    }

    public int getActiveCount() {
        return activeCount;
    }

    public void addActiveCount(int activeCount) {
        this.activeCount += activeCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(String creativeId) {
        this.creativeId = creativeId;
    }

    public static CreativeData buildCreativeData(Integer adid, Collection<Map<String, Object>> creativeCollection) {
        CreativeData creativeData = new CreativeData();
        Integer total = 0;
        for (Map<String, Object> dataObject : creativeCollection) {
            Integer creativeCount = Integer.valueOf(dataObject.get("creativeCount").toString());
            if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.MEDIA_CONFIRMED.getCode()
                    || Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.THIRD_CONFIRMED
                            .getCode()) {
                creativeData.addActiveCount(creativeCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.INITIAL.getCode()) {
                creativeData.setUnActiveCount(creativeCount);
            }
            total += creativeCount;
            creativeData.setMediaSource(String.valueOf(dataObject.get("source")));
        }
        creativeData.setAdid(adid);
        creativeData.setTotalCount(total);
        return creativeData;
    }

}
