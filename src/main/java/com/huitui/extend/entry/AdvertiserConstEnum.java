package com.huitui.extend.entry;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Author: gang.pan Date: 5/25/16 Time: 12:35 Mail: gang.pan@htmob.cn
 */
public enum AdvertiserConstEnum {
    JJ(472885640, "jjService"), ELONG_TRAVEL(388089858, "elongService"), ELONG_HOTEL(629502680,
            "elongService"), WAR_AND_ORDER(1121598982, "warAndOrderService"), SAN_GUO(638670990,
                    "sanguoService"), MOMO_JINWUTUAN(1019160422, "momoService"), LAN_XI_NIU(960194511,
                            "lanxiniuService"), YU_JIAN_QING_YUAN(1088150831, "ziLongService"), JIA_XIA_QING_YUAN(
                                    107380, "qqService"), MENG_HUAN_ZHU_XIAN(107382, "qqService"), WANG_QUAN_ZHI_ZHENG(
                                            1129166907,
                                            "lingyouhuyuService"), SHI_HUN(1061860553, "momoService"), RE_XUE_CHUAN_QI(
                                                    107332, "qqService"), YU_LONG_ZAI_TIAN(107373,
                                                            "qqService"), XIAN_JIAN_QI_XIA_ZHUAN(1105823499,
                                                                    "shiquService"), YI_TIAN_TU_LONG_JI(1038743400,
                                                                            "shiquService"), HUO_YING_REN_ZHE(107345,
                                                                                    "qqService"), TIAN_TIAN_KU_PAO(
                                                                                            107148,
                                                                                            "qqService"), WAR_AND_ORDER_NEW(
                                                                                                    1179626509,
                                                                                                    "talkingDataService");

    private static Map<Integer, String> mediaServiceMap = Maps.newHashMap();
    static {
        for (AdvertiserConstEnum item : AdvertiserConstEnum.values()) {
            mediaServiceMap.put(item.getAppId(), item.getServiceName());
        }
    }

    private int appId;
    private String serviceName;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    AdvertiserConstEnum(int appId, String serviceName) {
        this.appId = appId;
        this.serviceName = serviceName;
    }

    public static String getServiceNameBySource(int appId) {
        return mediaServiceMap.get(appId);
    }
}
