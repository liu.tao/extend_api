package com.huitui.extend.entry;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * Author: gang.pan Date: 5/25/16 Time: 12:35 Mail: gang.pan@htmob.cn
 */
public enum AdvertiserChartEnum {
    JJ(472885640, "jj"), ELONG_TRAVEL(388089858, "elongTravel"), ELONG_HOTEL(629502680, "elongHotel"), WAR_AND_ORDER(
            1121598982, "warAndOrder"), MOMO_JINWUTUAN(1019160422,
                    "陌陌劲舞团"), MENG_HUAN_ZHU_XIAN(107382, "梦幻诛仙"), SHI_HUN(1061860553, "侍魂OL");

    public static Map<Integer, String> adNameMap = Maps.newHashMap();

    static {
        for (AdvertiserChartEnum item : AdvertiserChartEnum.values()) {
            adNameMap.put(item.getAdid(), item.getAdName());
        }
    }

    private int adid;
    private String adName;

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    AdvertiserChartEnum(int adid, String adName) {
        this.adid = adid;
        this.adName = adName;
    }

    public static String getAdNameById(int adid) {
        return adNameMap.get(adid);
    }
}
