package com.huitui.extend.entry;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * 处理日期和时间的工具类。 实现了不同格式的日期/时间转换，日期/时间之间的计算，以及固定日期格式的判断。
 * <p/>
 * <p>
 * 日期时间格式命范参考Microsoft Developer Network中标准日期和时间格式字符串的命名规范。
 * <p/>
 * <p>
 * 更多有关日期和时间格式字符串命名规范，请参考: <a href="http://msdn.microsoft.com/zh-cn/library/az4se3k1(v=vs.110).aspx#FullDateLongTime">
 *
 * @author fanghui.zhang
 * @version 1.0 2013-11-19
 */

public class DateTimeUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtils.class);

    private static final long KILO = 1000L;

    private static final long DAY_MILLIS = 24 * 60 * 60 * 1000;

    private static final long HOUR_MILLIS = 60 * 60 * 1000;

    private static final long MINUTE_MILLIS = 60 * 1000;

    public static final String ORDER_SOURCE_DATE_FORMAT = "yyyyMMddHHmm";

    /**
     * 常规日期时间模式(长时间): yyyy-MM-dd HH:mm:ss
     */
    public static final String GENERAL_DATETIME_PATTERN_LT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 常规日期时间模式(短时间): yyyy-MM-dd HH:mm
     */
    public static final String GENERAL_DATETIME_PATTERN_ST = "yyyy-MM-dd HH:mm";
    public static final String GENERAL_DATETIME_PATTERN_ST_NO_SPACE = "yyyy-MM-ddHH:mm";

    /**
     * 常规日期时间模式(长时间)(中文): yyyy年MM月dd日 HH:mm:ss
     */
    public static final String GENERAL_DATETIME_PATTERN_LT_CN = "yyyy年MM月dd日 HH:mm:ss";
    /**
     * 常规日期时间模式(长时间)(中文): yyyy年MM月dd日 HH:mm
     */
    public static final String GENERAL_DATETIME_PATTERN_LT_CN_NO_SECOND = "yyyy年MM月dd日 HH:mm";
    /**
     * 常规日期时间模式(清除格式): yyyyMMddHHmmss
     */
    public static final String GENERAL_DATETIME_PATTERN_CLEAR = "yyyyMMddHHmmss";
    public static final String PATTEN_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String PATTEN_LOLON_yyyyMMddHHmmss = "yyyyMMddHH:mm:ss";
    public static final String PATTEN_yyyyMMddHH = "yyyyMMddHH";
    /**
     * 常规日期时间模式(清除格式并简化): yyMMddHHmmss
     */
    public static final String GENERAL_DATETIME_PATTERN_SIMPLIFIED = "yyMMddHHmmss";
    public static final String PATTEN_yyMMddHHmmss = "yyMMddHHmmss";
    /**
     * 短日期模式: yyyy-MM-dd
     */
    public static final String SHORT_DATE_PATTERN = "yyyy-MM-dd";

    public static final String PATTEN_yyyy_MM_dd = "yyyy-MM-dd";
    /**
     * 短日期模式(中文): yyyy年MM月dd日
     */
    public static final String SHORT_DATE_PATTERN_CN = "yyyy年MM月dd日";

    /**
     * 短日期模式(清除格式): yyyyMMdd
     */
    public static final String SHORT_DATE_PATTERN_CLEAR = "yyyyMMdd";
    public static final String PATTEN_yyyyMMdd = "yyyyMMdd";
    /**
     * 长时间模式: HH:mm:ss
     */
    public static final String LONG_TIME_PATTERN = "HH:mm:ss";

    /**
     * 短时间模式: HH:mm
     */
    public static final String SHORT_TIME_PATTERN = "HH:mm";
    /**
     * 日期模式: MM-dd
     */
    public static final String MONTH_PATTERN = "yyyy-MM";
    /**
     * 模式: MM月dd日 HH:mm
     */
    public static final String MONTH_DAY_HOUR_PATTERN = "MM月dd日 HH:mm";
    /**
     * 模式：MM月dd日HH:mm
     */
    public static final String MONTH_DAY_HOUR_NO_BLANK = "MM月dd日HH:mm";
    /**
     * 模式 MM月dd日
     */
    public static final String MONTH_DAY = "MM月dd日";
    /**
     * 模式：HHmm
     */
    public static final String HOUR_MINUTE = "HHmm";

    public static final String PATTEN_yyyy_MM_dd_HH_mm_ss_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

    /** 数据库中日期默认时间 */
    public static final Date DB_DEFAULT_TIME = parseDate("2000-01-01 00:00:00", GENERAL_DATETIME_PATTERN_LT);

    public static String getCstTime() {
        DateFormat cstFormat = new SimpleDateFormat("EEE d-MMM-yyyy HH:mm:ss z", Locale.US);
        TimeZone gmtTime = TimeZone.getTimeZone("GMT+8");
        cstFormat.setTimeZone(gmtTime);
        return cstFormat.format(new Date())+" (CST)";
    }

    /**
     * 判断日期字符串是否为常规日期模式，这里的常规日期模式指yyyy-MM-dd HH:mm:ss，不考虑长时间和短时间的区别。
     * <p/>
     * <p>
     * General Date Pattern: yyyy-MM-dd HH:mm:ss
     *
     * @param dateStr 需要进行常规日期模式判断的字符串
     * @return true 如果字符串是常规日期模式
     */
    public static boolean isGeneralDatePattern(String dateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(GENERAL_DATETIME_PATTERN_LT);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(dateStr);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 判断日期字符串是否为短日期模式。
     * <p/>
     * <p>
     * Short Date Pattern: yyyy-MM-dd
     *
     * @param dateStr 需要进行短日期模式判断的字符串
     * @return true 如果字符串是短日期模式
     */

    public static boolean isShortDatePattern(String dateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(SHORT_DATE_PATTERN);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(dateStr);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static boolean isLegalDate(String dateStr, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(dateStr);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 按指定格式解析日期字符串，返回一个日期对象。
     *
     * @param dateStr 需要进行格式解析的字符串
     * @param pattern 需要解析成的目标格式
     * @return 按照pattern解析的日期对象
     */

    public static Date parseDate(String dateStr, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        try {
            return dateFormat.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 按指定格式格式化日期对象，返回日期字符串。
     *
     * @param date 需要进行格式化的日期对象
     * @param pattern 需要格式化的目标格式
     * @return 按照pattern格式化的日期字符串
     */

    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        try {
            return dateFormat.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 按指定格式格式化当前日期，返回当前日期字符串。
     *
     * @param pattern 需要格式化的目标格式
     * @return 按照pattern格式化的当前日期字符串
     */

    public static String formatCurrentDate(String pattern) {
        Date date = new Date();
        return formatDate(date, pattern);
    }

    /**
     * 获取当前日期（Date中时分秒都为0）
     *
     * @return
     */
    public static Date getCurrentDateWithoutTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 指定起始格式和目标格式，格式化日期字符串，返回目标格式的日期字符串。
     *
     * @param source 需要进行格式化的日期字符串
     * @param fromPattern 起始格式
     * @param toPattern 目标格式
     * @return 目标格式的日期字符串
     */

    public static String formatDate(String source, String fromPattern, String toPattern) {
        Date date = parseDate(source, fromPattern);
        return formatDate(date, toPattern);
    }

    /**
     * 将Integer秒数时间戳解析为指定格式的时间字符串。
     *
     * @param second Integer时间戳-精确到秒
     * @return 常规日期模式的时间字符串
     */

    public static String parseTimeSeconds(Integer second, String pattern) {
        long millis = second.longValue() * KILO;
        Date date = new Date(millis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        return dateFormat.format(date);
    }

    /**
     * 将long型毫秒解析指定格式的的时间字符串。
     *
     * @param millis long型时间戳-精确到毫秒
     * @param pattern 目标格式
     * @return 常规日期模式的时间字符串
     */

    public static String parseTimeMillis(long millis, String pattern) {
        Date date = new Date(millis);
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        return dateFormat.format(date);
    }

    /**
     * 将Integer秒数时间戳转化为long型毫秒。
     *
     * @param second Integer时间戳-精确到秒
     * @return long型时间戳-精确到毫秒
     */

    public static long secondToMillis(Integer second) {
        return second.longValue() * 1000L;
    }

    /**
     * 将long型毫秒转化成Integer秒数。
     *
     * @param millis long型时间戳-精确到毫秒
     * @return Integer时间戳-精确到秒
     */

    public static Integer millisToSecond(long millis) {
        return Integer.valueOf((int) (millis / KILO));
    }

    public static Integer millisToSecond(Date time) {
        return millisToSecond(time.getTime());
    }

    /**
     * 将时间字符串按指定格式转化成毫秒。
     *
     * @param dateStr 需要进行转化的时间字符串
     * @param pattern 目标格式
     * @return long型时间戳-精确到毫秒
     */

    public static long toTimeMillis(String dateStr, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setLenient(false);
        try {
            Date date = dateFormat.parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将当前时间格式化为毫秒
     *
     * @return long型时间戳-精确到毫秒
     */

    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 将当前时间格式化为Integer时间戳-精确到秒
     *
     * @return Integer时间戳-精确到秒
     */

    public static Integer currentTimeSeconds() {
        return Integer.valueOf((int) (currentTimeMillis() / KILO));
    }

    /**
     * 得到现在的年份
     *
     * @return int格式的年份
     */

    public static int getYear() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        return year;
    }

    /**
     * 得到现在的月份
     *
     * @return int格式的月份
     */

    public static int getMonth() {
        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH) + 1;
        return month;
    }

    /**
     * 得到现在具体是一个月中的第几天
     *
     * @return int格式的天数
     */

    public static int getDay() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    /**
     * 得到现在的小时数
     *
     * @return int格式的小时数
     */

    public static int getHour() {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    public static int getHour(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    public static int getHour(String time, String pattern) {
        Date timeDate = parseDate(time, pattern);
        if (timeDate == null) {
            throw new RuntimeException("invalid arguments format");
        }
        return getHour(timeDate);
    }

    /**
     * 得到现在的分钟
     *
     * @return int格式的分钟数
     */

    public static int getMinute() {
        Calendar cal = Calendar.getInstance();
        int min = cal.get(Calendar.MINUTE);
        return min;
    }

    public static int getMinute(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.MINUTE);
        return hour;
    }

    // 得到现在的分钟数
    public static int getMinutes() {
        Date now = new Date();
        return 60 * DateTimeUtils.getHour(now) + DateTimeUtils.getMinute(now);
    }

    // 得到分钟数,如 02:10 得到的是2*60+10=130
    public static int getMinutes(String minuteStr, String pattern) {
        Date beginTime = parseDate(minuteStr, pattern);
        return 60 * getHour(beginTime) + getMinute(beginTime);
    }

    public static int getMinute(String time, String pattern) {
        Date timeDate = parseDate(time, pattern);
        if (timeDate == null) {
            throw new RuntimeException("invalid arguments format");
        }
        return getMinute(timeDate);
    }

    /**
     * 得到现在的秒数
     *
     * @return int格式的秒数
     */

    public static int getSecond() {
        Calendar cal = Calendar.getInstance();
        int sec = cal.get(Calendar.SECOND);
        return sec;
    }

    /**
     * 在一个日期基础上加/减年数
     *
     * @param myDate 需要进行运算的日期
     * @param amount 需要加减的年数，正整数则为加，负整数则为减
     * @return 进行运算后的日期对象
     */

    public static Date addYear(Date myDate, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.YEAR, amount);
        return cal.getTime();
    }

    /**
     * 在一个日期的基础上加/减天数
     *
     * @param myDate 需要进行运算的日期
     * @param amount 需要加减的天数，正整数则为加，负整数则为减
     * @return 进行运算后的日期对象
     */

    public static Date addDay(Date myDate, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.DAY_OF_MONTH, amount);
        return cal.getTime();
    }

    /**
     * @param date
     * @param amount
     * @return
     */
    public static Date addMonth(Date date, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, amount);
        return cal.getTime();
    }

    /**
     * 在一个日期的基础上加/减小时
     *
     * @param myDate 需要进行运算的日期
     * @param amount 需要加减的小时数，正整数则为加，负整数则为减
     * @return 进行运算后的日期对象
     */

    public static Date addHour(Date myDate, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.HOUR_OF_DAY, amount);
        return cal.getTime();
    }

    /**
     * 在一个日期的基础上加/减分钟
     *
     * @param myDate 需要进行运算的日期
     * @param amount 需要加减的分钟数，正整数则为加，负整数则为减
     * @return 进行运算后的日期对象
     */

    public static Date addMin(Date myDate, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.MINUTE, amount);
        return cal.getTime();
    }

    /**
     * 在一个日期的基础上加/减秒
     *
     * @param myDate 需要进行运算的日期
     * @param amount 需要加减的秒数，正整数则为加，负整数则为减
     * @return 进行运算后的日期对象
     */

    public static Date addSec(Date myDate, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.SECOND, amount);
        return cal.getTime();
    }

    /**
     * 获取某个日期对象的后一天
     *
     * @param date 需要进行运算的日期
     * @return 该日期的后一天，日期对象
     */

    public static Date getNextDate(Date date) { // 原方法为getNextDate 5.工具方法
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(date);
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
            return date;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取和今天日期的偏移天数
     *
     * @param offset 偏移天数
     */
    public static Date getOffsetDate(Date date, Integer offset) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(date);
            cal.add(Calendar.DATE, offset);
            date = cal.getTime();
            return date;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取两个日期对象之间的间隔天数
     *
     * @param startDate 起始日期对象
     * @param endDate 结束日期对象
     * @return 两个日期对象之间的间隔天数，int格式
     */

    public static int getDaysBetween(Date startDate, Date endDate) { // 原方法为getIntervalDay 5.工具方法
        Calendar scal = Calendar.getInstance();
        scal.setTime(startDate);
        scal.set(Calendar.HOUR_OF_DAY, 0);
        scal.set(Calendar.MINUTE, 0);
        scal.set(Calendar.SECOND, 0);
        scal.set(Calendar.MILLISECOND, 0);
        Calendar ecal = Calendar.getInstance();
        ecal.setTime(endDate);
        ecal.set(Calendar.HOUR_OF_DAY, 0);
        ecal.set(Calendar.MINUTE, 0);
        ecal.set(Calendar.SECOND, 0);
        ecal.set(Calendar.MILLISECOND, 0);
        long duration = ecal.getTimeInMillis() - scal.getTimeInMillis();
        return (int) (duration / DAY_MILLIS);
    }

    /**
     * 获取两个date之间的间隔小时数
     *
     * @param startDate 起始日期对象
     * @param endDate 结束日期对象
     * @return 两个日期对象之间的间隔小时数，int格式
     */

    public static int getHoursBetween(Date startDate, Date endDate) {
        Calendar scal = Calendar.getInstance();
        scal.setTime(startDate);
        Calendar ecal = Calendar.getInstance();
        ecal.setTime(endDate);
        long duration = ecal.getTimeInMillis() - scal.getTimeInMillis();
        return (int) (duration / HOUR_MILLIS);
    }

    public static int getMinutesBetween(Date startDate, Date endDate) {
        Calendar scal = Calendar.getInstance();
        scal.setTime(startDate);
        Calendar ecal = Calendar.getInstance();
        ecal.setTime(endDate);
        long duration = ecal.getTimeInMillis() - scal.getTimeInMillis();
        return (int) (duration / MINUTE_MILLIS);
    }

    /**
     * 获取当前时间戳
     *
     * @returnn
     */
    public static Integer now() {
        return Integer.valueOf((int) (System.currentTimeMillis() / 1000));
    }

    /**
     * 将毫秒转化为了Integer时间戳
     *
     * @param millis
     * @return
     */
    public static Integer toTs(long millis) {
        return Integer.valueOf((int) (millis / 1000));
    }

    /**
     * 将Integer时间戳转化为millis
     *
     * @param ts
     * @return
     */
    public static Long toMills(Integer ts) {
        return ts.longValue() * 1000L;
    }

    /**
     * 获取时间的周,英文
     *
     * @param date
     * @return
     */
    public static String getWeekOfDate(Date date) {
        SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
        return dateFm.format(date);
    }

    public static int dayForWeek(String dateStr) {
        Date date = parseDate(dateStr, PATTEN_yyyy_MM_dd);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayForWeek = 0;
        if (c.get(Calendar.DAY_OF_WEEK) == 1) {
            dayForWeek = 7;
        } else {
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }
        return dayForWeek;
    }

    public static String getWeek(Date date) {
        String[] weeks = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return weeks[week_index];
    }

    public static int expireDateSeconds(Date date) {
        long result = date.getTime() - System.currentTimeMillis();
        result = result > 0 ? result : 0;
        return (int) (result / 1000);
    }

    // 得到昨天
    public static Date getYesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }

    public static Date minusMinute(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, -minutes);
        return calendar.getTime();
    }

    /**
     * 获取指定天之前unix戳
     *
     * @param
     * @returnn
     */
    public static Integer day(int index) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, index);
        return Integer.valueOf((int) (cal.getTimeInMillis() / 1000));
    }

    /**
     * @param
     * @return Date
     * @Title: expireDate
     * @Description:
     */
    public static Date expireDate(int dayNum) {
        return new Date(currentTimeMillis() + dayNum * 24 * 60 * 60 * 1000);
    }

    /**
     * @param dateInfo->2015-01-20
     * @return Date
     * @Title: nextDayStartTime
     * @Description: 获取某一天第二天凌晨时间
     */
    public static Date nextDayStartTime(String dateInfo) {
        Date thisDate = parseDate(formatDate(dateInfo, SHORT_DATE_PATTERN, GENERAL_DATETIME_PATTERN_LT),
                GENERAL_DATETIME_PATTERN_LT);
        return addDay(thisDate, 1);
    }

    public static int beforeNextDayStartSeconds(String dateInfo) {
        Date nextDay = nextDayStartTime(dateInfo);
        int result = (int) (nextDay.getTime() - System.currentTimeMillis()) / 1000;
        return result > 0 ? result : 0;
    }

    /**
     * @param dateInfo->2015-01-20
     * @return Date
     * @Title: nextDayStartTime
     * @Description: 获取某一天第二天凌晨时间
     */
    public static Date nextDayStartTime(Date dateInfo) {
        Date thisDate = parseDate(formatDate(dateInfo, GENERAL_DATETIME_PATTERN_LT), GENERAL_DATETIME_PATTERN_LT);
        return addDay(thisDate, 1);
    }

    public static Date zeroTime(Date dateInfo) {
        return parseDate(formatDate(dateInfo, PATTEN_yyyy_MM_dd), PATTEN_yyyy_MM_dd);

    }

    public static boolean isDate(String dttm, String format) {
        if (dttm == null || dttm.isEmpty() || format == null || format.isEmpty()) {
            return false;
        }

        if (format.replaceAll("'.+?'", "").indexOf("y") < 0) {
            format += "/yyyy";
            DateFormat formatter = new SimpleDateFormat("/yyyy");
            dttm += formatter.format(new Date());
        }

        DateFormat formatter = new SimpleDateFormat(format);
        formatter.setLenient(false);
        ParsePosition pos = new ParsePosition(0);
        Date date = formatter.parse(dttm, pos);

        if (date == null || pos.getErrorIndex() > 0) {
            return false;
        }
        if (pos.getIndex() != dttm.length()) {
            return false;
        }

        if (formatter.getCalendar().get(Calendar.YEAR) > 9999) {
            return false;
        }

        return true;
    }

    /**
     * 毫秒数转成xx小时xx分钟xx秒xx毫秒
     */
    public static String format(long time) {
        int hour = (int) (time / 3600000);
        time = time - hour * 3600000;
        int minute = (int) (time / 60000);
        time = time - minute * 60000;
        int second = (int) (time / 1000);
        time = time - second * 1000;
        return hour + "小时" + minute + "分钟" + second + "秒" + time + "毫秒";
    }

    public static Date unixTimeToDate(long unixTime) {
        return new Date(unixTime * KILO);
    }

    /**
     * 获取两个日期之间的全部日期
     *
     * @param startDateStr 格式为"yyyy-MM-dd"
     * @param endDateStr 格式为"yyyy-MM-dd"
     * @return betweenDates
     */
    public static List<String> getBetweenDates(String startDateStr, String endDateStr) {
        List<String> betweenDates = Lists.newArrayList();
        try {
            Date startDate = parseDate(startDateStr, SHORT_DATE_PATTERN);
            Date endDate = parseDate(endDateStr, SHORT_DATE_PATTERN);
            while (startDate.compareTo(endDate) <= 0) {
                betweenDates.add(formatDate(startDate, SHORT_DATE_PATTERN));
                startDate = addDay(startDate, 1);
            }
        } catch (Exception e) {
            LOGGER.error("getBetweenDates error", e);
        }
        return betweenDates;
    }
}