package com.huitui.extend.entry;

/**
 * Author: gang.pan Date: 5/17/16 Time: 20:00 Mail: gang.pan@htmob.cn
 */
public enum HttpStatusEnum {

    SUCCESS(200, "请求成功"), AUTH_FAIL(201, "权限错误"), SERVER_ERR(202, "服务器错误"), ILLEGAL_REQUEST(203, "非法请求"), REQUEST_ERROR(
            204, "请求错误"), ;

    HttpStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(final int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }
}