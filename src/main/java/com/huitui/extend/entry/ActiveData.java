package com.huitui.extend.entry;

import com.alibaba.fastjson.annotation.JSONField;
import com.huitui.sms.ActiveSmsVO;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Author: gang.pan Date: 7/1/16 Time: 12:29 Mail: gang.pan@htmob.cn
 */
public class ActiveData {

    public Integer getUnActiveCount() {
        return unActiveCount;
    }

    public void setUnActiveCount(Integer unActiveCount) {
        this.unActiveCount = unActiveCount;
    }

    private int activeCount;
    private int registerCount;
    private int orderCount;
    private int unActiveCount;
    private int concealCount;
    private int thirdConfirmCount;
    private int totalCount;
    private BigDecimal activePercent;
    private BigDecimal concealPercent;
    public static final String ACTIVE_TASK_LOCK_KEY = "active_data_today";
    public static final String ACTIVE_SMS_LOCK = "active_sms_lock";

    private Integer adid;
    private String adName;

    private String mediaSource;

    public Integer getThirdConfirmCount() {
        return thirdConfirmCount;
    }

    public void setThirdConfirmCount(Integer thirdConfirmCount) {
        this.thirdConfirmCount = thirdConfirmCount;
    }

    // 计划投放的数量
    @JSONField(serialize = false)
    private int planCount;

    // 是否发送短信
    private boolean isSendSms;

    private int smsCount;

    public int getRegisterCount() {
        return registerCount;
    }

    public void setRegisterCount(int registerCount) {
        this.registerCount = registerCount;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public String getMediaSource() {
        return mediaSource;
    }

    public void setMediaSource(String mediaSource) {
        this.mediaSource = mediaSource;
    }

    public int getSmsCount() {
        return smsCount;
    }

    public void setSmsCount(int smsCount) {
        this.smsCount = smsCount;
    }

    public int getPlanCount() {
        return planCount;
    }

    public void setPlanCount(int planCount) {
        this.planCount = planCount;
    }

    public boolean isSendSms() {
        return isSendSms;
    }

    public void setSendSms(boolean sendSms) {
        isSendSms = sendSms;
    }

    public Integer getAdid() {
        return adid;
    }

    public String buildCacheKey(String date) {
        return date + "_" + mediaSource;
    }

    public String buildSecondCacheKey(String time) {
        return time;
    }

    public String buildRealTimeKey() {
        return adid + "_today";
    }
    public String buildCreativeKey(){
        return adid + "_creative";
    }

    public static String buildRealTimeKey(String adid) {
        return adid + "_today";
    }

    public void setActivePercent(BigDecimal activePercent) {
        this.activePercent = activePercent;
    }

    public void setConcealPercent(BigDecimal concealPercent) {
        this.concealPercent = concealPercent;
    }

    public void setAdid(Integer adid) {
        this.adid = adid;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public int getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(int activeCount) {
        this.activeCount = activeCount;
    }

    public Integer getConcealCount() {
        return concealCount;
    }

    public void setConcealCount(Integer concealCount) {
        this.concealCount = concealCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public double getConcealPercent() {

        int activeTotalCount = 0;
        if (activeCount > 0) {
            activeTotalCount = activeCount;
        }
        if (concealCount > 0) {
            activeTotalCount += concealCount;
        }
        if (totalCount == 0) {
            return 0;
        }
        return new BigDecimal(activeTotalCount).divide(new BigDecimal(totalCount), 2, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }

    public double getActivePercent() {
        if (totalCount == 0) {
            return 0;
        }
        return new BigDecimal(activeCount + thirdConfirmCount + registerCount + orderCount)
                .divide(new BigDecimal(totalCount), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static ActiveData buildActiveData(Integer adid, Collection<Map<String, Object>> activeCollection) {
        ActiveData activeData = new ActiveData();
        Integer total = 0;
        for (Map<String, Object> dataObject : activeCollection) {
            Integer statusCount = Integer.valueOf(dataObject.get("statusCount").toString());
            if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.MEDIA_CONFIRMED.getCode()) {
                activeData.setActiveCount(statusCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.CONCEAL.getCode()) {
                activeData.setConcealCount(statusCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.INITIAL.getCode()) {
                activeData.setUnActiveCount(statusCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.THIRD_CONFIRMED
                    .getCode()) {
                activeData.setThirdConfirmCount(statusCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.REGISTER.getCode()) {
                activeData.setRegisterCount(statusCount);
            } else if (Integer.parseInt(dataObject.get("status").toString()) == MissionStatusEnum.PAY.getCode()) {
                activeData.setOrderCount(statusCount);
            }
            total += statusCount;
            activeData.setMediaSource(String.valueOf(dataObject.get("source")));
        }
        activeData.setAdid(adid);
        activeData.setAdName(AdvertiserChartEnum.getAdNameById(adid));
        activeData.setTotalCount(total);
        return activeData;
    }

    public String buildSmsString() {
        ActiveSmsVO activeSmsVO = new ActiveSmsVO();
        activeSmsVO.setAppName(this.getAdName());
        activeSmsVO.setActiveData(String.valueOf(this.getActiveCount()));
        activeSmsVO.setConcealData(String.valueOf(this.getConcealCount()));
        activeSmsVO.setActivePercent(String.valueOf(this.getActivePercent()));
        activeSmsVO.setTotal(String.valueOf(this.getTotalCount()));
        return activeSmsVO.toString();
    }
}
