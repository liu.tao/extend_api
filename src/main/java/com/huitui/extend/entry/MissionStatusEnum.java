package com.huitui.extend.entry;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Author: gang.pan Date: 5/19/16 Time: 15:11 Mail: gang.pan@htmob.cn
 */
public enum MissionStatusEnum {
    INITIAL("初始", 0), NORMAL("正常", 1), NOT_FINISHED("进行中", 2), FINISHED("完成", 3), THIRD_CONFIRMED("广告主确认完成",
            4), MEDIA_CONFIRMED("渠道确认完成", 5), ADVERTISER_FAILED("回调广告主失败",
                    6), CONCEAL("隐藏", 7), REMAIN("付费", 8), REGISTER("注册", 9), PAY("留存", 10);
    private int code;
    private String desc;

    private static Map<Integer, String> missionStatusMap = Maps.newHashMap();

    static {
        for (MissionStatusEnum item : MissionStatusEnum.values()) {
            missionStatusMap.put(item.getCode(), item.getDesc());
        }
    }

    MissionStatusEnum(String desc, int code) {
        this.code = code;
        this.desc = desc;
    }

    public static boolean containsObject(int code) {
        return missionStatusMap.containsKey(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(final int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }
}