package com.huitui.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.huitui.extend.entry.ActiveData;
import com.huitui.extend.entry.DateTimeUtils;
import com.huitui.sms.ActiveSmsVO;
import com.huitui.sms.SmsMessage;
import com.taobao.api.ApiException;

/**
 * Author: gang.pan Date: 6/30/16 Time: 17:14 Mail: gang.pan@htmob.cn
 */
public class GsonTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(GsonTest.class);

    @Test
    public void testGson() {
        ActiveSmsVO activeSmsVO = new ActiveSmsVO();
        activeSmsVO.setActiveData("1200");
        activeSmsVO.setActivePercent("70%");
        activeSmsVO.setAppName("jj");
        LOGGER.info("gson={}", activeSmsVO.toString());
    }

    @Test
    public void testJson() {
        String string = "{\"activeCount\":235,\"activePercent\":0.58,\"adName\":\"jj\",\"adid\":472885640,"
                + "\"concealCount\":52,\"concealPercent\":0.71,\"smsTime\":0,\"totalCount\":406,\"unActiveCount\":118}";
        System.out.println(JSONObject.parseObject(string, ActiveData.class));

    }

    @Test
    public void testSms() {
        try {
            SmsMessage.sendSms("", "18611127760", "{\"name\":\"sohu\"}", "SMS_22395055");
        } catch (ApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCstTime() {
        LOGGER.info("cstTime={}", DateTimeUtils.getCstTime());
    }
}
