package com.huitui.test;

import com.huitui.mail.MailService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;

/**
 * Author: gang.pan Date: 7/18/16 Time: 16:09 Mail: gang.pan@htmob.cn
 */
public class MailServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceTest.class);

    @Test
    public void testMail() {
        try {
            MailService.sendMail("1040773823@qq.com", "慧推收", "广告主代理注册");
        } catch (Exception e) {
            LOGGER.info("mail_err", e);
        }

    }

    @Test
    public void testMailFile() {
        try {
            String path = "/tmp/dailyOriginalityData.xls";
            MailService.sendMailAttachment("1040773823@qq.com,tao.liu@htmob.cn", "当日创意信息", "广告主代理注册", path);
        } catch (Exception e) {
            LOGGER.info("mail_err", e);
        }

    }
}
